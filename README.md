# vue3-admin

#### 介绍

vue3 入门，一步一步完善，`Vite + Vue3 + TypeScript + Element-plus`，后端部分搭配（[Node 后台管理系统](https://gitee.com/hlshare/express_mongoDB)）

#### 软件架构

```javascript

├── public/
└── src/
    ├── api/                       // 接口请求目录
    ├── assets/                    // 静态资源目录
    ├── common/                    // 通用类库目录
    ├── components/                // 公共组件目录
    ├── config/                    // 配置信息，如：baseURL
    ├── router/                    // 路由配置目录
    ├── store/                     // 状态管理目录
    ├── style/                     // 通用 CSS 目录
    ├── utils/                     // 工具函数目录
    ├── views/                     // 页面组件目录
    ├── App.vue
    ├── main.ts
    ├── env.d.ts
├── tests/                         // 单元测试目录
├── index.html
├── tsconfig.json                  // TypeScript 配置文件
├── vite.config.ts                 // Vite 配置文件
└── package.json

```

#### 安装教程

npm install

#### 使用说明

1.  npm run dev 本地启动
2.  npm run build 打包
3.  npx prettier --write . 格式化全部文件

#### 参考资料

[从 0 开始手把手带你搭建一套规范的 Vue3.x 项目工程环境](https://juejin.cn/post/6951649464637636622)
