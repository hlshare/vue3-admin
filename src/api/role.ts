import axios from '@/utils/axios'

// 列表
export const rolesPage = (info: object) => {
  return axios.request({
    url: 'role/page',
    data: info,
    method: 'post'
  })
}
