import axios from '@/utils/axios'

// 商品列表
export const goodsPage = (info: object) => {
  return axios.request({
    url: 'food/page',
    data: info,
    method: 'post'
  })
}
// 商品添加/修改 (传id为修改，不传id为新增)
export const goodsAddOrUpdate = (info: object) => {
  return axios.request({
    url: 'food/addOrUpdate',
    data: info,
    method: 'post'
  })
}
// 商品删除
export const goodsDel = (info: object) => {
  return axios.request({
    url: 'food/del',
    data: info,
    method: 'post'
  })
}
