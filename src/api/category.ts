import axios from '@/utils/axios'

// 商品分类列表
export const categoryPage = (info: object) => {
  return axios.request({
    url: 'foodType/page',
    data: info,
    method: 'post'
  })
}
// 商品修改
export const categoryUpdate = (info: object) => {
  return axios.request({
    url: 'foodType/update',
    data: info,
    method: 'post'
  })
}
// 商品添加
export const categoryAdd = (info: object) => {
  return axios.request({
    url: 'foodType/add',
    data: info,
    method: 'post'
  })
}
// 商品删除
export const categoryDel = (info: object) => {
  return axios.request({
    url: 'foodType/del',
    data: info,
    method: 'post'
  })
}
