import axios from '@/utils/axios'

// 登录
export const login = (info: object) => {
  return axios.request({
    url: 'user/login',
    data: info,
    method: 'post'
  })
}
// 列表
export const usersPage = (info: object) => {
  return axios.request({
    url: 'user/page',
    data: info,
    method: 'post'
  })
}
// 添加
export const usersAdd = (info: object) => {
  return axios.request({
    url: 'user/add',
    data: info,
    method: 'post'
  })
}
// 分配角色
export const usersUpdateRole = (info: object) => {
  return axios.request({
    url: 'user/updateRole',
    data: info,
    method: 'post'
  })
}
// 修改用户状态
export const usersUpdateState = (info: object) => {
  return axios.request({
    url: 'user/updateState',
    data: info,
    method: 'post'
  })
}
// 删除
export const usersDel = (info: object) => {
  return axios.request({
    url: 'user/del',
    data: info,
    method: 'post'
  })
}
// 修改密码
export const usersUpdatePs = (info: object) => {
  return axios.request({
    url: 'user/updatePs',
    data: info,
    method: 'post'
  })
}
