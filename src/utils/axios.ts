import Axios from 'axios'
import { ElMessage } from 'element-plus'
import { baseURL } from '@/config/index'
import router from '@/router/index'
import { getLocal } from '@/utils/local'

const axios = Axios.create({
  baseURL,
  timeout: 20000, // 请求超时 20s
  headers: {
    Accept: "application/json, text/plain, */*",
    "Content-Type": "application/json"
  }
})

// 前置拦截器（发起请求之前的拦截）
axios.interceptors.request.use(
  (config: any) => {
    /**
     * 根据你的项目实际情况来对 config 做处理
     * 这里对 config 不做任何处理，直接返回
     */
    const token: any = getLocal('token')
    config['headers']['token'] = token
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

// 后置拦截器（获取到响应时的拦截）
axios.interceptors.response.use(
  (response) => {
    /**
     * 根据你的项目实际情况来对 response 和 error 做处理
     * 这里对 response 和 error 不做任何处理，直接返回
     */
    if (response.status !== 200) {
      ElMessage.error('服务器异常')
    } else if (response.data.code !== 200) {
      ElMessage.error(response.data.msg)
      setTimeout(() => {
        router.push('/login')
      }, 3000);
    }
    return response
  },
  (error) => {
    if (error.response && error.response.data) {
      const code = error.response.status
      const msg = error.response.data.message
      ElMessage.error(`[Axios Error]` + msg)
    } else {
      ElMessage.error(`${error}`)
    }
    return Promise.reject(error)
  }
)

export default axios
