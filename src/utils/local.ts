const setLocal = (key: string, value: string) => {
  localStorage.setItem(key, value)
}

const getLocal = (key: string) => {
  return localStorage.getItem(key)
}

export { setLocal, getLocal }
